Source: jhbuild
Section: devel
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Emilio Pozuelo Monfort <pochu@debian.org>, Jeremy Bícha <jbicha@ubuntu.com>, Jordi Mallach <jordi@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pkg-config,
               trang <!nocheck>,
               yelp-tools
Standards-Version: 4.4.0
Vcs-Git: https://salsa.debian.org/gnome-team/jhbuild.git
Vcs-Browser: https://salsa.debian.org/gnome-team/jhbuild
Homepage: https://wiki.gnome.org/Projects/Jhbuild

Package: jhbuild
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
	 python3-distutils,
Recommends: git-core,
            patch,
            wget | curl,
            autoconf,
            automake,
            gettext,
            meson,
            pkg-config
Suggests: graphviz
Description: flexible build script for package collections
 Jhbuild is a program that can be used to pull a number of modules from
 Git, CVS, Subversion, Bazaar and other types of repositories or from
 tarballs and build them in the correct order.  Unlike some build
 scripts, jhbuild lets you specify what modules you want built and it
 will then go and build those modules plus dependencies.
 .
 Although jhbuild was originally developed to build GNOME, it has
 since been extended to work with other projects as well.
 Extending it to handle new modules is usually trivial assuming the
 build infrastructure matches the other modules it handles.
